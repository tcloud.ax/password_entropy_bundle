<?php

declare(strict_types=1);

namespace PasswordEntropyBundle\Exception;

use Exception;

class PasswordEntropyBundleException extends Exception
{
}