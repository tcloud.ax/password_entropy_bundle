<?php

declare(strict_types=1);

namespace PasswordEntropyBundle\Exception;

use PasswordEntropyBundle\Exception\PasswordEntropyBundleException;

class PasswordIsEmptyException extends PasswordEntropyBundleException
{
}